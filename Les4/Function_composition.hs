module Main where

{-
    Function composition

    Het punt wordt hier gebruikt om functions na mekaar aan te roepen, right associative.

    De function 'words' krijgt een string als input
        "hey there man"
    en maakt er een lijst van woorden van
        ["hey","there","man"].

    Daarop wordt 'map reverse' toegepast : map past de function reverse toe op elk element van de lijst.
        ["yeh","ereht","nam"]

    De function 'unwords' doet het tegenovergestelde van 'words'.
        "yeh ereht nam"

    Let op : je geeft hier wel het type van het argument op (String), maar je gebruikt het niet in de definition.
    Is niet nodig, omdat je alleen maar een nieuwe function maakt.
    Haskell weet dat hij words moet toepassen op het eerste argument.
-}

main :: IO ()
main = do
    putStrLn("Begin")

    let sentence = "Machine learning is a subfield of computer science that evolved from the study of pattern recognition and computational learning theory in artificial intelligence. Machine learning explores the study and construction of algorithms that can learn from and make predictions on data."
    let wordsInSentence = words sentence
    print wordsInSentence

    let wordsInSentenceUC = words sentence

    let backToSentence = unwords wordsInSentence
    print backToSentence

    -- Function application met spatie = left associative
    print ("Hello")
    print "Hello"

    -- Function application met punt = right associative
    let neg1 = map (\ x -> negate (abs x)) [5, -3, -6, 7, -3, 2, -19, 24]
    print neg1
    let neg2 = map (negate . abs) [5, -3, -6, 7, -3, 2, -19, 24]
    print neg2

    -- Function application met $ = right associative, syntactic sugar
    print (take 5 (filter even [1..20]))
    -- Is hetzelfde als :
    print (take 5 $ filter even [1..20])

    putStrLn("End")