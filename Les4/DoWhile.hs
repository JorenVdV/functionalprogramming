module Main where

main :: IO ()
main = do

    -- Herhaal iets tot wanneer er een lege lijn wordt ingegeven.
    putStrLn("Geef je voornaam in (lege lijn om te stoppen) : ")
    line <- getLine
    if null line
        then return ()       -- Return een IO action die niets doet.
        else do
            putStrLn("Je naam : " ++ line)
            main
