module Oefeningen where
import System.Random
import System.IO.Unsafe
import Data.List.Split
import Data.Char


getRChar ::  Char
getRChar = 
	unsafePerformIO (randomRIO ('a','z') )

randomStr :: Int -> String
randomStr count =
	take count $ randomRs ('a','z') $ unsafePerformIO newStdGen

printStrWithTabs :: String -> IO()
printStrWithTabs str = 
	putStrLn (foldl1 (++) [x++"\t" | x <- (chunksOf 1 str)])

getRandomNumbers :: Int -> [Int]
getRandomNumbers count =
	take count $ randomRs (1,1000) $ unsafePerformIO newStdGen

countIntInList :: Int -> [Int] -> Int
countIntInList x y = 
	length (filter (==x) y)

vowelstoCapital :: String -> String
vowelstoCapital str =
	[if elem x "aeiou" then toUpper x else x | x <- str]

countVowels :: String -> (Int, Int)
countVowels str = 
	(length (filter (\x -> elem x "aeiou") str),
		length (filter (\x -> not (elem x "aeiou")) str))

filterChars :: String -> [String] -> [String]
filterChars chars strings = 
	filter (\x -> elem (head x) chars) strings

filterNotChars :: String -> [String] -> [String]
filterNotChars chars strings = 
	filter (\x -> not (elem (head x) chars)) strings

countSpaces :: String -> Int
countSpaces str = 
	foldl1 (+) [if x == ' ' then 1 else 0 | x <- str]

main :: IO()
main = do
    
    --putStrLn("Begin")

    -- Oefening 1
--    let chartoguess = getRChar
--    putStr "Guess the char:"
--    guess <- getChar
--    putStrLn ""
--    if guess == chartoguess
--    	then do
--    		putStrLn "Rigth\n"
--   		return()
--   	 else main

	-- Oefening 2
--	printStrWithTabs (randomStr 5)
	
	-- Oefening 3
--	print (countIntInList (head (getRandomNumbers 1)) (getRandomNumbers 1000))
	str <- readFile "Runaway.txt"
	writeFile "RunawayInCaps.txt" (vowelstoCapital str)
	writeFile "RunawayVowelsAndConsonants" (show (countVowels str))
	

	let str1 = "Machine learning is a subfield of computer science that evolved from the study of pattern recognition and computational learning theory in artificial intelligence. Machine learning explores the study and construction of algorithms that can learn from and make predictions on data."
	let str2 = splitOn " " "Machine learning is a subfield of computer science that evolved from the study of pattern recognition and computational learning theory in artificial intelligence. Machine learning explores the study and construction of algorithms that can learn from and make predictions on data."
	let str3 = chunksOf 1 "Machine learning is a subfield of computer science that evolved from the study of pattern recognition and computational learning theory in artificial intelligence. Machine learning explores the study and construction of algorithms that can learn from and make predictions on data."
	print(filterChars "Mm" str2)
	print(filterChars "aeiouAEIOU" str2)
	print(filterNotChars "aeiouAEIOU" str2)
	print(countSpaces str1)
	print(filterChars "!@#$%^&*(){}[]:;<>,./?" str1)

	putStrLn("End")




















