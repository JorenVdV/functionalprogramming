module Main where

import System.IO
import Data.Char

{-
    Om van schijf te lezen, moet je eerst System.IO importeren.
-}

main :: IO ()
main = do
    putStrLn("Begin")
    putStrLn("")

    -- Eerste manier
    handle <- openFile "Runaway.txt" ReadMode
    contents <- hGetContents handle
    putStr contents
    hClose handle

    -- Tweede manier, met lambda syntax.
    withFile "Runaway.txt" ReadMode 
        ( 
            \handle 
            -> 
            do
                contents <- hGetContents handle
                putStr contents
        )

    -- Omdat IO zo veel voorkomt : 3 utility functions : readFile, writeFile, appendFile.
    -- Voor toUpper moeten we Data.Char importeren.
    writeFile "RunawayNew.txt" "And yet his joy is empty and sad."

    contents <- readFile "Runaway.txt"
    writeFile "RunawayInCaps.txt" (map toUpper contents)
    
    appendFile "RunawayInCaps.txt" "\n\nAnd yet his joy is empty and sad."

    putStrLn("\n")
    putStrLn("End")