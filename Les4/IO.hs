module Main where

main :: IO ()
main = do
    putStrLn("Begin")
    putStrLn ""

    -- Input lezen doe je met <-, niet met let
    putStr("\tGeef je voornaam in :\t\t")
    firstName <- getLine
    putStr("\tGeef je familienaam in :\t")
    lastName <- getLine
    putStrLn("\tJe naam = " ++ firstName ++ " " ++ lastName)
    putStrLn ""

    -- Assignments doe je met de gewone = . Wisten we al.
    let name = "Casper Peeters"
    putStrLn ("\t" ++ name)

    -- Output character per character
    putChar '\t'
    putChar 'a'
    putChar '\t'
    putChar 'z'
    putChar '\n'

    putStrLn ""
    putStrLn("End")