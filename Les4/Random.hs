module Main where

import System.Random


{-
    Om random waarden te genereren, moet je eerst System.Random importeren.

    De function 'random' wordt gebruikt om random getallen te genereren.
    
        random :: (RandomGen g, Random a) => g -> (a, g)

    Aan de linkerkant van het => teken staat de context, zogenaamde type-variabelen
    Aan de rechterkant van het => teken staat de type definitie,
    die gebruikt maakt van de elementen in de context.
    
    Je moet dit dus zo lezen :
        random is een function waaraan je
            een randomGenerator
        meegeeft
            en die een tuple teruggeeft met een randomgetal en een nieuwe Randomgenerator.
-}

main :: IO ()
main = do
    putStrLn("Begin")
    putStrLn("")

    -- mkStdGen is een function die een random generator instance returnt.
    -- Aan mkStdGen moet je een seed meegeven.
    -- Aan random geven we dan die StdGen mee, en we moeten ook het output type specifiëren,
    -- een tuple in dit geval.
    let gen = (mkStdGen 1000)
    let r = random (gen) :: ( Int , StdGen )
    print (fst (r))

    -- Een lijst van random numbers krijg je met randoms, met de s van sequence.
    -- randoms returnt een oneindige lijst, dus we moeten er de eerste n van nemen.
    -- We moeten ook specifiëren welk type waarden er gereturned wordt :: [Int].
    -- Let op : function application met $, RIGHT associative.
    let randomList = take 5 $ randoms (gen) :: [Int]
    print randomList

    -- Een lijst van random numbers uit een range krijg je met randomRs, 
    -- met de R van range en de s van sequence.
    let randomList = take 10 $ randomRs (1, 20) (gen) :: [Int]
    print randomList

    -- Alweer een shortcut : newStdGen. Neemt telkens een nieuwe seed.
    generator <- newStdGen
    print (take 10 $ (randomRs ('a', 'z') generator) )

    putStrLn("")
    putStrLn("End")