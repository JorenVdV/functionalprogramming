 module Hangman where 

import Data.List.Split
import Data.List
import System.IO  
import System.Random
import Control.Monad (replicateM)

{-
    Hangman is is a game where the player has to guess a word in a number of tries.
-}

-- ***************************************************************************************************
main :: IO()
main =  do
    -- Open 'words.txt' in readmode. The source file contains 163977 words, 1 word per line.
    -- Read the full file.
    text <- readFile "words.txt"
    let textsplit = splitOn "\n" text

    -- Use a random generator to get 1 word from the list of words.
    --    Use the function 'lines' to break a string up into a list of strings at newline characters.
    --    Get the number of words (1 word per line). This will be necessary for the random generator.
    num <- randomRIO (0 , (genericLength textsplit)) :: IO Integer
    let word = head(drop (fromIntegral num) textsplit)
    print word
    
    -- Give a cue to the player, using a '_' for each character in the word.
    --    Use map to change each character of the word to '_'.

    -- Determine the max number of tries = 10.

    -- Give feedback to the player, telling him how many characters the word has.

    -- Call the function 'play' with
    --    the word (not to be shown to the player)
    --    the currentSolution
    --    the number of the current try

    putStrLn "End"


-- ***************************************************************************************************
-- The function 'play' gets 3 arguments and has return type IO().
-- Input
--     Arg1 = the word,
--     Arg2 = the solution found so far,
--     Arg3 = the current number of tries.
-- Output
--    IO()
-- Uses
--    try
--    play (recursively)

--play :: String -> String -> Int -> IO()
    -- Feedback to player.
    
    -- Read the next guess.
    -- Take the first character (for safety, to catch typo's).

    -- Process the guess by calling the function 'try', which returns a tuple (Int, String).
    -- Give feedback to the player, showing the current solution.
    
    -- React to the result.
    --     If the first element == -1, end of application, game over.
    --     If not, call play recursively with
    --         the target word
    --         the current solution and
    --         the number of the current try.


-- ***************************************************************************************************
-- The function 'try' gets 4 arguments and returns a tuple of an Int and a String.
-- Input
--    Arg1 = the word.
--    Arg2 = the solution found so far. 
--    Arg3 = the character to be processed.
--    Arg4 = the current number of tries.
-- Output
--    A tuple (Int, String)
--        The Int = the current number of tries.
--        The String = the current solution or a message to the player.
-- Uses
--    testWord

--try :: String -> String -> Char -> Int -> (Int, String)
    -- Use a guard.
    -- Condition 1 : the current solution does not contain a '_' character.
    --    Return -1 and an appropriate message, game over.
    -- Condition 2 : the number of tries == 0, so we have to stop.
    --    Return -1 and a message saying the player has lost, game over + the solution.
    -- Otherwise :
    --    Call the function 'testWord', to process the current character. 


-- ***************************************************************************************************
-- The function 'testWord' gets 4 arguments and returns a tuple of an Int and a String.
-- Input
--    Arg1 = the word.
--    Arg2 = the solution found so far. 
--    Arg3 = the character to be processed.
--    Arg4 = the current number of tries.
-- Output
--    A tuple (Int, String)
--        The Int = the current number of tries.
--        The String = the current solution.

--testWord :: String -> String -> Char -> Int -> (Int, String)
    -- Use a guard.
    -- Condition 1 :
    --    If the character is part of the word, return a tuple, containing
    --        the number of the current try
    --        the result of zipping 'word' with 'currentSolution', using the character.
    -- Otherwise :
    --    If the character is not part of the word,
    --    decrease numberOfCurrentTry with 1 and proceed with the current solution.


{- Information on zipWith :

zipWith : unifies 2 lists, item by item, applying a function to each item.

    Arg1 = the function expressed as a lambda expression. This function will be used to zip the 2 Strings 
    (word and currentSolution).

    (\c s -> if c == letter then c else s)

        \        : beginning of lambda expression
        c s      : arguments of the lambda expression at the lefthand side of ->
                 : c is the current item in word
                 : s is the corresponding item in currentSolution
        ->       : assignment operator

        If c equals the letter, then return the letter.
        If not, return the corresponding letter of currentSolution.

    Arg2 = the word to be guessed.
    Arg3 = the current solution, found so far.

By running through the word and the current solution, character by character, we merge the new letter with the solution.

-}

--
-- Extra : pass the number of characters to be found with every try.
--
