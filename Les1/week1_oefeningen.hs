module Oefeningen where

{- 1. Schrijf en test de function sumOfList
Hier is het type:
sumOfList :: [Integer] -> Integer

Gebruik recursie.

Als list_1 = [1, 2, 3, 4, 5], dan is de som 15. -}
sumOfList :: [Integer] -> Integer
sumOfList [] = 0
sumOfList (x:xs) = 
	x + sumOfList (xs)

sumOfList' :: [Integer] -> Integer
sumOfList' xs = 
	foldl (+) 0 xs

-- Test: sumOfList [1..5]

{- 2. Schrijf en test de function productOfList. Altijd eerst het type opgeven, dat geldt voor alle oefeningen. -}
productOfList :: [Integer] -> Integer
productOfList [] = 0
productOfList [x] = x
productOfList (x:xs) = 
	x * productOfList xs

productOfList' :: [Integer] -> Integer
productOfList' [] = 0
productOfList' (x:xs) = 
	foldl (*) x xs
-- Test: sumOfList [1..5]

{- Gebruik terug de list van oefening 1 en print de volgende elementen door de list operatoren te gebruiken. Je roept eigenlijk gewoon voorgeprogrammeerde functions aan:-}
--Het eerste element
--print(head [1..10])
-- Het laatste element
--print(last [1..10])
-- De lengte van de list
--print(length [1..10])
-- De list in omgekeerde volgorde
--print(reverse [1..10])
-- Maak een nieuwe variabele firstPart en steek er de eerste 2 getallen van list in. Met 1 function call.
--let firstPart = take 2 [1..10]
--print(firstPart)
-- Maak een nieuwe variabele secondPart en steek er de laatste 3 getallen van list in
--let secondPart = take 3 [1..10]
--print(secondPart)
-- Maak een nieuwe lijst list_2 = [6, 7, 8, 9, 10]. Concateneer de 2 lists in een derde, list_3.
--print([1..5]++[6..10])
-- •	Maak een nieuwe lijst list_4 die de 4 laatste elementen van list_1 bevat en de eerste 4 van list_2.
--print( (drop ((length [1..5])-4) [1..5] ++ take 4 [6..10]))

{- 4. Schrijf en test de function evenElementsInList. Je kan hiervoor de function even gebruiken. Je geeft de lijst [1,2,3,4,5,6,7,8,9,10] mee en de lijst [2,4,6,8,10] wordt gereturned -}
evenElementsInList :: [Integer] -> [Integer]
evenElementsInList [] = []
evenElementsInList (x:xs) = 
	if even x
		then [x] ++ evenElementsInList xs
		else evenElementsInList xs

-- oplossing met lijst filter
evenElementsInList' :: [Integer] -> [Integer]
evenElementsInList' xs = 
	filter even xs

-- Test: evenElementsInList [1..10]

{- 5.	Schrijf en test de function divisibleBy3InList. 
Een mogelijke oplossing : schrijf eerst een function met het volgende type :
divisibleBy3 :: Integer -> [Integer]

Deze function geeft dus een lijst terug. 
Die lijst gebruik je in divisibleBy3InList om te concateneren -}
divisibleBy3 :: Integer -> Bool
divisibleBy3 x = 
	mod x 3 == 0

divisibleBy3InList :: [Integer] -> [Integer]
divisibleBy3InList [] = []
divisibleBy3InList (x:xs) = 
	if divisibleBy3 x
		then [x] ++ divisibleBy3InList xs
		else divisibleBy3InList xs

-- oplossing met lijst filter
divisibleBy3InList' :: [Integer] -> [Integer]
divisibleBy3InList' xs =
	filter (\x -> mod x 3 == 0) xs

-- Test: divisibleBy3InList [1..10]

{- Extra Oefeningen 1. Schrijf een function die telt hoeveel keer een bepaald character voorkomt in een string. 
Een String is een lijst van chars : [Char]. Dit is het function type: 
numberOfOccurrencesChar :: String -> Char -> Int -> Int-}
numberOfOccurrencesChar  :: [Char] -> Char -> Integer
numberOfOccurrencesChar [] _ = 0
numberOfOccurrencesChar (x:xs) a = 
	if (x==a)
		then 1 + numberOfOccurrencesChar xs a
		else numberOfOccurrencesChar xs a

numberOfOccurrencesChar'  :: [Char] -> Char -> Integer
numberOfOccurrencesChar'  xs x= 
	fromIntegral (length (filter (==x) xs))

-- Test: numberOfOccurrencesChar "this is a string" 'i'

{- Extra oefeningne 2. 1.	Schrijf een function die telt hoeveel keer een bepaalde substring voorkomt in een string.
Dit is het function type: 
numberOfOccurrencesString :: String -> String -> Int -> Int-}
numberOfOccurrencesString :: String -> String -> Integer
numberOfOccurrencesString [] _ = 0
numberOfOccurrencesString xs ys = 
	if (take (length ys) xs == ys)
		then 1 + numberOfOccurrencesString (drop (length ys) xs) ys
		else numberOfOccurrencesString (drop 1 xs) ys
-- Test: numberOfOccurrencesString "This is a string" "is"

