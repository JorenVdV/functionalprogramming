module Main where

add2To :: Integer -> Integer
add2To x = x+2

add2ToList :: [Integer] -> [Integer]
add2ToList [] = []
add2ToList (x:xs) = 
    [add2To x] ++ add2ToList (xs)

add2ToList' :: [Integer] -> [Integer]    
add2ToList' xs = 
	map (+2) xs

divisibleBy3 :: Integer -> Bool
divisibleBy3 x = 
	mod x 3 == 0

sumOfList :: [Integer] -> Integer
sumOfList [] = 0
sumOfList (x:xs) = 
	x + sumOfList (xs)

productOfList :: [Integer] -> Integer
productOfList [] = 0
productOfList [x] = x
productOfList (x:xs) = 
	x * productOfList xs

evenElementsInList :: [Integer] -> [Integer]
evenElementsInList [] = []
evenElementsInList (x:xs) = 
	if even x
		then [x] ++ evenElementsInList xs
		else evenElementsInList xs

evenElementsInList' :: [Integer] -> [Integer]
evenElementsInList' xs = 
	filter even xs





divisibleBy3InList :: [Integer] -> [Integer]
divisibleBy3InList [] = []
divisibleBy3InList (x:xs) = 
	if divisibleBy3 x
		then [x] ++ divisibleBy3InList xs
		else divisibleBy3InList xs

divisibleBy3InList' :: [Integer] -> [Integer]
divisibleBy3InList' xs =
	filter (\x -> mod x 3 == 0) xs

numberOfOccurrencesChar  :: [Char] -> Char -> Integer
numberOfOccurrencesChar  [] _ = 0
numberOfOccurrencesChar  (x:xs) a = 
	if (x==a)
		then 1 + numberOfOccurrencesChar xs a
		else numberOfOccurrencesChar xs a

numberOfOccurrencesChar'  :: [Char] -> Char -> Integer
numberOfOccurrencesChar'  xs x= 
	fromIntegral (length (filter (==x) xs))


numberOfOccurrencesString :: String -> String -> Integer
numberOfOccurrencesString [] _ = 0
numberOfOccurrencesString xs ys = 
	if (take (length ys) xs == ys)
		then 1 + numberOfOccurrencesString (drop (length ys) xs) ys
		else numberOfOccurrencesString (drop 1 xs) ys

-- Type prototype:loa
main :: IO()
-- Definition
main = do 
    putStrLn("Start of program")
    let list = [1..5]
    putStrLn("\tThis is a tab.")
    print list

    let a = head(list)
    print a

    let b = tail(list)
    print b

    print (reverse list)

    let reverseList = reverse (list)
    print(reverseList !! 3)

    print (add2To (5))
    print (add2To (a))

    let newlist = add2ToList (list)
    print (newlist)

    let firstList = take 2 list
    print firstList

    let secondList = drop 2 list
    print secondList

    let list2 = [6..10]
    print (init list2)

    let thirdlist = firstList ++ secondList
    print thirdlist

    let evenHead = even (head list)
    print evenHead

    let d = divisibleBy3 8
    print d

    putStrLn("End of examples")
    print(sumOfList([1..10]))
    print(productOfList[1..10])
    print(head [1..10])
    print(last [1..10])
    print(length [1..10])
    print(reverse [1..10])
    let firstPart = take 2 [1..10]
    print(firstPart)
    let secondPart = take 3 [1..10]
    print(secondPart)
    print([1..5]++[6..10])
    print( (drop ((length [1..5])-4) [1..5] ++ take 4 [6..10]))


    putStrLn("End of program")