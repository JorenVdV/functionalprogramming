module Main where

-- 2 ways to make a record in Haskell

{-
    BookInfo = naam van het nieuw type : type constructor.
        Type constructor MOET met een hoofdletter beginnen.
    Book = de naam van de value constructor.
        Value constructor MOET met een hoofdletter beginnen.
    Na de value constructor volgen de components van het type.
        Int         Identifier
        String      Title
        [String]    Namen van de auteurs
    deriving (Show) is nodig om de interpreter te vertellen hoe/dat hij de fields moet kunnen printen.
-}

data BookInfo = Book Integer String [String] deriving (Show)

{-
    Person = naam van het nieuw type : type constructor.
        Type constructor MOET met een hoofdletter beginnen.
    Person = de naam van de value constructor (mag dus dezelfde zijn).
        Value constructor MOET met een hoofdletter beginnen.
    Na de value constructor volgen de components van het type, elk met de fieldName en zijn type.
        firstName   String
        lastName    String
        age         Int
        height      Float
    deriving (Show) is nodig om de compiler te vertellen hoe/dat hij de fields moet kunnen printen.
-}

data Person = Person
    {
        firstName       :: String,
        lastName        :: String,
        age             :: Int,
        height          :: Float
    } deriving (Show)


main :: IO ()
main = do
    putStrLn("Begin")

    let bookInfo = Book 9780135072455 "Algebra of Programming" ["Richard Bird", "Oege de Moor"]
    putStrLn("\tPrinting the full record")
    putStr("\t")
    print bookInfo

    let boy = Person {firstName = "Casper", lastName = "Peeters", age = 20, height = 2.01}
    putStrLn("\n\tPrinting the full record")
    putStr("\t")
    print boy

    putStrLn("\n\tPrinting field 'firstName' of the record")
    putStr("\t")
    print (firstName boy)

    putStrLn("\n\tAssignment of field 'firstName' of the record")
    putStr("\t")
    let fName = firstName boy
    print fName

    putStrLn("End")

{-
    Accessing fields of a record

    When a datatype is defined with records, Haskell automatically defines FUNCTIONS with the same name
    as the record to act as accessors. In this case 'firstName' is the accessor for the firstName field
    (it has type Person -> String).
-}