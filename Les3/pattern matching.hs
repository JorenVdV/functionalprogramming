module Main where

{-
    Pattern matching consists of specifying patterns to which some data should conform, 
    then checking to see if it does, and deconstructing the data according to those patterns.
-}

-- De underscore wordt gebruikt als placeholder : om het even wat, afhankelijk van de context.
-- Note : eerste vorm van error checking.
initials :: String -> String -> String
initials (firstLetter:_) (lastLetter:_) =
    [firstLetter] ++ ". " ++ [lastLetter] ++ "."
initials _ (lastLetter:_) =
    "You must give a first name !"
initials (firstLetter:_) _ =
    "You must give a last name !"

main :: IO ()
main = do
    putStrLn("Begin")

    putStr("\n\tinitials Casper Peeters =\t")
    print (initials "Casper" "Peeters")

    putStr("\tinitials \"\" Peeters =\t")
    print (initials "" "Peeters")

    putStr("\tinitials Casper \"\" =\t")
    print (initials "Casper" "")

    putStrLn("\nEnd")