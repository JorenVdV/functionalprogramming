module Main where

{-
    Een guard = een geneste if clause. Ook te vergelijken met een switch-case.

    Elke | test een condition.
    Als de condition true is, zal de function de waarde na het = teken returnen.
    Otherwise = een soort default.

    Let op : het = teken om de function te definiëren staat niet op de eerste lijn.
-}

typeOfInstrument :: String -> String
typeOfInstrument instrument
    | instrument == "Drums"    = "Ludwig"
    | instrument == "Guitar"   = "Fender"
    | instrument == "Keyboard" = "Yamaha DX 7"
    | otherwise                = "No instrument"

--  Gebruik van temp variabele met 'where'.
getMultiplier :: Int -> Double
getMultiplier number
    | result < 100 = 0.5
    | result < 200 = 0.75
    | result < 300 = 1.25
    | otherwise    = 1.0
    where result = number ^ 2

-- 'let in' constructie
cylinderSurface :: Double -> Double -> Double
cylinderSurface radius height =
    let sideArea = 2 * pi * radius * height
        topArea = pi * radius ^ 2
    in  sideArea + 2 * topArea


main :: IO ()
main = do
    putStrLn("Begin of program")

    putStr("\tDrums = ")
    print (typeOfInstrument "Drums")

    putStr("\tBass = ")
    print (typeOfInstrument "Bass")

    putStr("\tmultiplier 12 = ")
    print (getMultiplier 12)

    putStr("\tcylindersurface = ")
    print (cylinderSurface 4 18)

    putStrLn("End of program")
