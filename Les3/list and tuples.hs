module Main where

{-
    Lists bevatten elementen van hetzelfde type.

    Tuples zijn ook lists, maar kunnen elementen van verschillende types bevatten.
-}


main :: IO ()
main = do
    putStrLn("Begin")

    -- List concatenation, gebruik :
    let list1 = [1..20]
    putStr("\tlist1\t")
    print list1

    let list2 = 0:list1
    putStr("\tlist2\t")
    print list2

    -- Is hetzelfde als
    let list3 = [0] ++ list1
    putStr("\tlist3\t")
    print list3


    -- Tuples : met ronde haakjes
    -- Kan elementen van verschillende types bevatten
    let child = ("Casper", 12)
    putStr("\n\tchild =\t")
    print (child)
    putStr("\tfirst of child =\t")
    print (fst child)
    putStr("\tsecond of child =\t")
    print (snd child)

    -- Initialisatie van tuples met list comprehension
    -- Een lijst van getallen, met als snd een boolean die aangeeft of ze even zijn
    let numbersAndBool = [(n, even n) | n <- [1..20]]
    putStrLn("\n\tnumbersAndBool =\t")
    print numbersAndBool

    -- Een lijst van tuples ~ geneste forloop
    let numberTuples = [(a, b) | a <- [1..10], b <- [11 .. 20]]
    putStrLn("\n\tnumberTuples =\t")
    print numberTuples

    putStrLn("\nEnd")