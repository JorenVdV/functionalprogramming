module Main where

printLineByLine :: String -> IO ()
printLineByLine [] = do
    putStr("")
printLineByLine (c:cs) = do
    putStr("\t")
    print (c)
    printLineByLine (cs)

main :: IO ()
main = do
    putStrLn("Begin")

    let name = "Casper"
    putStrLn("\tname, each letter on a line =")
    printLineByLine name

    putStrLn("End")