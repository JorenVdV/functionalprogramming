module Oefeningen where

import Data.Char
import Data.List

-- Oefening 1
inRange :: Integer -> String
inRange x
	| x<=100 = "Range 0/100"
	| x<=200 = "Range 100/200"
	| x<=1000 = "Range 200/1000"
	| otherwise = "Out of range"

 -- oefening 2
trapeziumSurface :: Double -> Double -> Double -> Double
trapeziumSurface a b h = 
	let average = 0.5*(a+b)
	in average * h

-- oefening 3
coneVolume :: Double -> Double -> Double
coneVolume radiusBase height =
	let base = pi*radiusBase^2
	in 1/3*base*height

-- oefening 4
ageRange :: Int -> String
ageRange birthyear
	| age <= 25 = "Range 0/25"
	| age <= 50 = "Range 25/50"
	| age <= 75 = "Range 50/75"
	| age <= 100 = "Range 75/100"
	| otherwise = "Out of range" 
	where age = 2016 - birthyear

-- oefening 5
data Musician = Musician 
	{
		geboortejaar :: Integer,
		instrument :: String,
		groep :: String,
		lengte :: Float
	} deriving (Show)

-- let m = Musician {birthYear=1970,instrument="guitar",group="groep",length'=170.9}
-- birthYear m
-- instrument m
-- group m
-- length' m


-- Oefening 5
countVowels :: String -> [Int]
countVowels string = 
	let grouped = group (sort [x | x <- [toLower t | t <- string ] , elem x ['a','e','i','o','u']])
	in [ length x | x <- grouped]
		

countVowels' :: String -> [(Char,Int)]
countVowels' string = 
	let grouped = group (sort [x | x <- [toLower t | t <- string ] , elem x ['a','e','i','o','u']])
	in [ (head x,length x) | x <- grouped]

printvowels :: [(Char, Int)] -> IO ()
printvowels [] = do
    putStr("")
printvowels (x:xs) = do
	putChar (fst x)
	putStr "\t:\t"
	print (snd x)
	printvowels xs























