module Main where

{-
    Pattern matching komt heel veel voor, ook achter de schermen.
    Function 'describeList' is een goed voorbeeld.

    In de type definition staat het volgende:
        Aan de left hand side van => staat dat er wordt gewerkt met een element a dat kan geshowed worden.
        De function heeft 1 argument : een list van a elementen.
        De function returnt een String.

    Er zijn 4 mogelijkheden, die elk met pattern matching werken:
        Als je de lege lijst meegeeft, wordt die op het argument van versie 1 gematched.
        Als je een singleton meegeeft, dan wordt die gematched op een element gevolgd door de lege lijst.
        Als je een pair meegeeft, dan wordt dat gematched op de eerste elementen gevolgd door de lege lijst.
        In alle andere gevallen, wordt er gematched op 2 elementen gevolgd door om het even wat : _ .
-}

describeList :: (Show a) => [a] -> String  
describeList [] = "The list is empty"
describeList (x:[]) = "The list has one element: " ++ show x
describeList (x:y:[]) = "The list has two elements: " ++ show x ++ " and " ++ show y
describeList (x:y:_) = "This list is long. The first two elements are: " ++ show x ++ " and " ++ show y 

main :: IO ()
main = do
    putStrLn("Begin")

    putStr("\n\tdescribeList \"\" = ")
    print (describeList "")
    putStr("\tdescribeList ['a'] = ")
    print (describeList ['a'])
    putStr("\tdescribeList ['a', 'b'] = ")
    print (describeList ['a', 'b'])
    putStr("\tdescribeList ['a' .. 'z'] = ")
    print (describeList ['a' .. 'z'])

    putStrLn("End")