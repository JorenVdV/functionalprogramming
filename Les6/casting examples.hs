module Main where

import Casting	-- Must agree with the module name, NOT the file name.

main :: IO ()
main = do
    putStrLn("Begin")

    putStr("\tstringToInt \"56\" = ")
    print (stringToInt "56")

    putStr("\tdoubleToString 25.1234 = ")
    print (doubleToString 25.1234)

    putStrLn("End")
