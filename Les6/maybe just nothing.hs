﻿module Main where

{-

    Error handling in Haskell.

    Voorbeeld : sqrt van een negatief getal.

    Het type lees je als volgt:
        Aan de left hand side van => staat de context : er wordt gewerkt met een element a
        dat zowel floating point als ordered is. Die Ord hebben we nodig omdat we a
        vergelijken in de guard.

        Let op : die left hand side moét altijd tussen haakjes staan als er meer dan 1 eigenschap wordt opgegeven.
        
        De function krijgt zo'n element a mee en returnt Maybe a.

    Als een function Maybe returnt, dan MOET het ofwel een Just returnen, of een Nothing.
        De Just geeft een normaal resultaat.
        De Nothing vangt de error op.

-}

safeSqrt :: (Floating a, Ord a) => a -> Maybe a
safeSqrt number
    | number >= 0 = Just (sqrt number)
    | otherwise = Nothing


main :: IO ()
main = do
    putStrLn("Begin")

    -- Using sqrt function
    putStr("\tsqrt (64) = ")
    print (sqrt (64))
    putStr("\n\tsqrt (-64) = ")
    print (sqrt (-64))

    -- Safe sqrt of a negative number.
    putStr("\n\tsafe sqrt (-64) = ")
    print (safeSqrt (-64))

    putStrLn("End")