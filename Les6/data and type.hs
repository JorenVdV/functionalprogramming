module Main where

{-
    data : techniek om je eigen datastructuur te definiëren (zie week 3).
    type : vergelijkbaar met typedef van C++, om je eigen type te maken.

    case constructie = vergelijkbaar met een guard.
-}

data Color = Red | Green | Blue
type RGB = (Int, Int, Int)

translateColor :: Color -> RGB
translateColor color =
    case color of
        Red -> (255, 0, 0)
        Green -> (0, 255, 0)
        Blue -> (0, 0, 255)

main :: IO ()
main = do
    putStrLn("Begin")

    putStr("\ttranslateColor Blue = ")
    print (translateColor Blue)

    putStrLn("End")