module Casting where

-- This is a module which can be imported in other .hs files.

stringToInt :: String -> Int
stringToInt s = read s :: Int

stringToFloat :: String -> Float
stringToFloat s = read s :: Float

stringToDouble :: String -> Double
stringToDouble s = read s :: Double

intToString :: Int -> String
intToString i = show i :: String

floatToString :: Float -> String
floatToString f = show f :: String

doubleToString :: Double -> String
doubleToString d = show d :: String
