module Oefeningen where

data Color = Red | Green | Blue | Cornflowerblue | Darkgoldenrod
type RGB = (Int, Int, Int)

translateColor :: Color -> RGB
translateColor color =
    case color of
        Red -> (255, 0, 0)
        Green -> (0, 255, 0)
        Blue -> (0, 0, 255)
        Cornflowerblue -> (101, 156, 239)
        Darkgoldenrod -> (184,134,11)


stringColorMapper :: String -> [RGB]
stringColorMapper [] = []
stringColorMapper ('s':'c':'h':xs) = 
	translateColor Cornflowerblue : stringColorMapper(xs)
stringColorMapper ('o':'r':'r':xs)=
	translateColor Darkgoldenrod : stringColorMapper(xs)
stringColorMapper (x:xs) = 
	if isVowel x
		then translateColor Red : stringColorMapper(xs)
		else translateColor Green : stringColorMapper(xs)

isVowel :: Char -> Bool
isVowel c = 
	elem c "aeiouAEIOU"