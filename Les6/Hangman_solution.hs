module Main where

import System.IO
import System.Random

{-
    Hangman is a game where the player has to guess a word in a number of tries.
-}

-- The function 'main' has return type IO(), so it involves input/output.
main :: IO()
main =  do
    handle <- openFile "words.txt" ReadMode                 -- Open file in readmode. The source file contains 163977 words, 1 word per line.
    content <- hGetContents handle                          -- Read the full file.    
    gen <- newStdGen                                        -- Get a new random number generator for every run.

    let words = lines content                               -- lines : breaks a string up into a list of strings at newline characters.
    let n = length words                                    -- Get the number of words (1 word per line). Will be used by the random generator.

    let (index, _) = randomR (0, n) gen :: (Int, StdGen)    -- randomR : takes a range (lo,hi) and a random number generator gen, and returns a random value uniformly distributed in the closed interval [lo,hi].
                                                            --           returns a random Int and a new randomgenerator.
                                                            -- index contains a random Int.
                                                            -- _ can be anything. Used to capture the new random generator.
                                                
    let word = words !! index                               -- word contains a random word from the list.

    let currentSolution = map (\c -> '_') word              -- lambda expression : for each c in word, replace c by _.
                                                            -- map the function to word.
                                                            -- currentSolution = a line of underscores, equal to the number of characters in word.
                                                            -- currentSolution is given as a clue to the player.

    let numberOfCurrentTry = 20                             -- numberOfCurrentTry = the maximum number of guesses.
                                                            -- will be decreased with every try, until reaching 0.

    putStrLn ("The word has " ++ show (length word) ++ " characters.")  -- Feedback to player.

    play word currentSolution numberOfCurrentTry                        -- Start a game.

-- The function 'play' gets 3 arguments and has return type IO().
-- Input
--     Arg1 = the word.
--     Arg2 = the solution found so far.
--     Arg3 = the current number of tries.
-- Output
--    IO()
-- Uses
--    try
--    play (recursively)

play :: String -> String -> Int -> IO()
play word currentSolution numberOfCurrentTry = do
    putStrLn (show (toGuessLetters word currentSolution) ++ " letters left to guess\nAttempt " ++ show numberOfCurrentTry ++ ", give a character:")        -- Feedback to player.
    line <- getLine                                           -- Read the guess.
    let letter = head line                                    -- Take the first character (safety).

    let result = try word currentSolution letter numberOfCurrentTry       -- Process the guess by calling the function try,
                                                                          -- which returns a tuple (Int, String).
    putStrLn (snd result)                                    -- Feedback to player, showing the current solution.

    if (fst result == -1)                                    -- React to the result.
        then
            return ()                               -- If the first element == -1, end of application, game over.
        else
            play word (snd result) (fst result)     -- If not, call play with 
                                                        --    the target word
                                                        --    the current solution and
                                                        --    the number of the current try.

-- The function 'try' gets 4 arguments and returns a tuple of an Int and a String.
-- Input
--     Arg1 = the word.
--     Arg2 = the solution found so far. 
--     Arg3 = the character to be processed.
--     Arg4 = the current number of tries.
-- Output
--    A tuple (Int, String)
--        The Int = the current number of tries.
--        The String = the current solution or a message to the player.
-- Uses
--    testWord
try :: String -> String -> Char -> Int -> (Int, String)
try word currentSolution letter numberOfCurrentTry
    | '_' `notElem` currentSolution  = (-1, "Nice job !")                                         -- If there are no more _ characters in the current solution, return (-1, "Nice job !"). Game over.
    | numberOfCurrentTry == 0        = (-1, "You lost ... The word was " ++ word)                 -- If the number of tries == 0, return (-1, "You lost ..."). Game over.
    | otherwise                      = testWord word currentSolution letter numberOfCurrentTry    -- Otherwise, call the function testWord, to process the current character. 

-- The function 'testWord' gets 4 arguments and returns a tuple of an Int and a String.
-- Input
--    Arg1 = the word.
--    Arg2 = the solution found so far. 
--    Arg3 = the character to be processed.
--    Arg4 = the current number of tries.
-- Output
--    A tuple (Int, String)
--        The Int = the current number of tries.
--        The String = the current solution.
testWord :: String -> String -> Char -> Int -> (Int, String)
testWord word currentSolution letter numberOfCurrentTry
    | letter `elem` word    = (numberOfCurrentTry - 1, zipWith (\c s -> if c == letter then c else s) word currentSolution)
                            -- If the character IS part of the word, return a tuple, containing
                            --    the number of the current try - 1
                            --    the result of zipping word with currentSolution, using the character.
    | otherwise             = (numberOfCurrentTry - 1, currentSolution)
                            -- If the character IS NOT part of the word, 
                            --    decrease numberOfCurrentTry with 1 and proceed with the current solution.

{- zipWith : unifies 2 lists, item by item, applying a function to each item.

Arg1 = the function expressed as a lambda expression. This function will be used to zip the 2 Strings (word and currentSolution).

(\c s -> if c == letter then c else s)

    \        : beginning of lambda expression
    c s      : parameters of the lambda expression at the lefthand side of ->
             : c is the current letter in word
             : s is the corresponding letter in currentSolution

    If c equals the letter, then return the letter.
    If not, return the corresponding letter of currentSolution.

Arg2 = the word to be guessed.
Arg3 = the current solution, found so far.

By running through the word and the current solution, character by character, we merge the new letter with the solution.
-}

--
-- Oefening : geef bij elke poging het aantal nog te zoeken letters mee.
--
-- Gebruik recursie, pattern matching en een guard.
--
toGuessLetters :: String -> String -> Int
toGuessLetters [] [] = 0
toGuessLetters (w:ord) (c:urrentSolution)
	| c == '_' = 1 + toGuessLetters ord urrentSolution
	| w == c = 0 + toGuessLetters ord urrentSolution

