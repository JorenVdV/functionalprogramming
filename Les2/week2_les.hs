module Main where

-- Type prototype:loa
main :: IO()
-- Definition
main = do
	putStrLn("Start of program")

	let list = [1..20]
	--filter even list

	--let evenList = filter even list
	--print evenList

	--filter odd list

	let filteredList = [x|x<-list,x<15,even x] 
	let mappedList = map (+3) list

	putStrLn("End of program")