module Oefeningen where
import Data.Char

-- 1. Gebruik filter om uit de getallen van 0 tot 100 alle getallen kleiner dan 50 te halen.
-- filter (>50) [1..100]

-- 2. Gebruik een list comprehension om uit de getallen van 0 tot 100 alle getallen deelbaar door 2 �n 4 te halen.
-- [x|x<-[1..100], even x, mod x 4 ==0]

-- 3. Gebruik een list comprehension om uit de zin "The quick brown fox jumps over the lazy dog" een lijst van de letters �u� die erin voorkomen te genereren. Print de lijst zelf en ook de lengte ervan
-- length([x| x<- "The quick brown fox jumps over the lazy dog", x=='u'])

-- 4. Gebruik een list comprehension om uit de zin "The Quick Brown Fox Jumps Over The Lazy Dog" een lijst van alle kleine letters die erin voorkomen te genereren. Gebruik hiervoor de function elem. Print de lijst zelf en ook de lengte ervan.
-- length([x| x<- "The quick brown fox jumps over the lazy dog", elem x ['a'..'z']])

-- 5. Gebruik een list comprehension om uit de zin "The quick brown fox jumps over the lazy dog" een lijst van alle klinkers die erin voorkomen te genereren. Gebruik hiervoor de function elem. Print de lijst zelf en ook de lengte ervan.
-- length([x| x<- "The quick brown fox jumps over the lazy dog", elem x ['a','e','i','o','u']])

-- 6. Gebruik map om van de lijst [1..20] een nieuwe lijst te maken waarin alle dubbels zitten
-- map (*2) [1..20]

-- 7. Gebruik een list comprehension om van de lijst van [1..20] de dubbels te laten zien. Nu enkel van de getallen in de lijst die deelbaar zijn door 10.
-- [x*2|x<-[1..20], mod x 10==0]

-- 8. Gebruik een list comprehension om van de lijst van [1..20] de dubbels te laten zien. Nu enkel van de getallen in de resulterende lijst, die deelbaar zijn door 10.
-- [y|y<-[x*2|x<-[1..20]], mod y 10==0]

-- 9. Gebruik een list comprehension om de zin "The quick brown fox jumps over the lazy dog" volledig in upper case te zetten. Je moet hiervoor een module importeren, nl Data.Char, en daarvan de function toUpper gebruiken.
-- :m Data.Char -- import Data.Char in file
-- [toUpper x|x<-"The quick brown fox jumps over the lazy dog"]

-- 10
-- Je schrijft eerst zelf 2 functions die een eenvoudige codering toepassen op characters. Hier zijn de types
-- code :: Char -> Char		-- Verhoogt de ascii waarde van het character met 5.
code :: Char -> Char
code x =
	chr ((ord x) + 5)
-- decode :: Char -> Char		-- Verlaagt de ascii waarde van het character met 5.
decode :: Char -> Char
decode x = 
	chr ((ord x) - 5)

-- Dan schrijf je een list comprehension die de zin van oefening 9 codeert.
-- [code x|x<-"The quick brown fox jumps over the lazy dog"]

-- En daarna een list comprehension die de nieuwe zin decodeert.
-- [decode x|x<-"Ymj%vznhp%gwt|s%kt}%ozrux%t{jw%ymj%qf\DEL~%itl"]

-- 11 Vertrek van de lijst [1..20]. Gebruik map om een nieuwe lijst te maken waarin bij elk getal 100 is bijgeteld. Gebruik nu foldl om die lijst te sommeren. Print het resultaat = 2210
-- foldl (+) 0 (map (+100) [1..20])

-- Extra oefening
sublists n xs = take (length xs - n + 1) $ sublists' n xs -- take all the sets that are long enough
    where sublists' _ [] = [[]] -- empty list returns empty list, will be filtered out in level above
          sublists' n xs@(_:rest) = take n xs : sublists' n rest 

numberOfOccurrencesString :: String -> String -> Integer
numberOfOccurrencesString [] _ = 0
numberOfOccurrencesString xs ys = 
	if (take (length ys) xs == ys)
		then 1 + numberOfOccurrencesString (drop 1 xs) ys
		else numberOfOccurrencesString (drop 1 xs) ys

numberOfOccurrencesString' :: String -> String -> Int
numberOfOccurrencesString' xs ys = 
	length [x | x <- sublists (length ys) xs, x == ys]

numberOfOccurrencesString'' :: String -> String -> Int
numberOfOccurrencesString'' xs ys = 
	length [x | x <- [take (length ys) (drop y xs)| y <- [0..((length xs)-(length ys))]], x==ys]























